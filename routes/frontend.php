<?php
$app->get('/home', function ($request, $response) {
    $db = $this->db;

    return $this->view->render($response, 'frontend/home.twig', [
        'page' => 'home',
        'keyword' => 'keyword',
        'description' => 'Deskripsi Web',
    ]);
});

$app->get('/about', function ($request, $response) {
    $db = $this->db;

    return $this->view->render($response, 'frontend/about.twig', [
        'page' => 'about',
        'keyword' => 'keyword',
        'description' => 'Tentang Web',
    ]);
});

$app->get('/contact', function ($request, $response) {
    return $this->view->render($response, 'frontend/contact.twig', [
        'page' => 'contact',
    ]);
});

$app->post("/submitcontact", function ($request, $response) {
//    print("sdfsd");
//    die;
    $data = $request->getParams();
    $db = $this->db;
    $submit = $_POST;
//    print_r($submit);
//    die;

    $db->insert("m_contact", $submit);
//    print_r($models);
//    die;
    return $this->view->render($response, 'frontend/submitcontact.twig', [
        'page' => 'submitcontact'
    ]);
});

$app->get('/shop', function ($request, $response) {
    $db = $this->db;
    $page = isset($_GET['page']) && $_GET['page'] != "" ? $_GET['page'] : 1;
    $hasil = createPagination('m_barang', 3, 9, $page, config('SITE_URL') . "/shop");
    $db->select("m_barang.id, m_barang.nama, m_barang.harga, m_barang_img.foto")
        ->from("m_barang_img")
        ->join("left join", "m_barang", "m_barang_img.m_barang_id=m_barang.id")
        ->groupby("m_barang.id");

    $models = $db->findAll();

    return $this->view->render($response, 'frontend/shop.twig', [
        'page' => 'shop',
        'data' => $hasil['resultData'],
        'pagination' => $hasil['pagination']
    ]);
});

$app->get('/detail_barang', function ($request, $response) {
    $db = $this->db;
    $params = $_GET;

    $db->select("
        m_barang.id, m_barang.nama, m_barang.deskripsi")
        ->from("m_barang")
        ->where("id", "like", $params["index"]);

    $models = $db->find();
//    print_r($models);
//    die;

    return $this->view->render($response, 'frontend/detail_barang.twig', [
        'page' => 'detail_barang',
        'data' => $models,
    ]);
});

$app->get('/blog', function ($request, $response) {
    $db = $this->db;

//    $waktu = date('Y-m-d H:i:s', $tanggal);
//    print_r($hari);
//    die;

    $db->select("m_artikel.id, m_artikel.judul, m_artikel.isi, m_artikel.detail_isi, m_artikel.tgl_publish, m_artikel.foto, m_artikel.url,
                m_kategori_artikel.nama AS kategori")
        ->from("m_artikel")
        ->join("left join", "m_kategori_artikel", "m_artikel.m_kategori_artikel_id=m_kategori_artikel.id");
    $models = $db->findAll();
    $newsList   = [];
    $defaultImg = config("SITE_IMG") . "no-image.png";

        foreach ($models as $key => $val) {
            $newsList[$key] = (array)$val;
            $firstImg = get_images($val->detail_isi);
            $newsList[$key]['firstImg'] = isset($firstImg[0]) ? $firstImg[0] : $defaultImg;
            $newsList[$key]['url'] = config('SITE_URL') . "frontend/blog/" . $val->url;
            $newsList[$key]['description'] = !empty($val->isi);
        }
//        print_r($newsList);
//        die;

    $db->select("m_artikel.id, m_artikel.judul, m_artikel.isi, m_artikel.tgl_publish, m_artikel.foto,
                m_kategori_artikel.nama AS kategori")
        ->from("m_artikel")
        ->join("left join", "m_kategori_artikel", "m_artikel.m_kategori_artikel_id=m_kategori_artikel.id")
        ->orderby("m_artikel.tgl_publish");
    $recent = $db->findAll();
//    print_r($recent);
//    die;

    date_default_timezone_set('Asia/Jakarta');
    $tanggal = strtotime('now');
    foreach ($recent as $key => $value) {
        $hari = strtotime($recent[$key]->tgl_publish);
        $detikperHari = strtotime('1 Day 30 second', 0);

        if (($tanggal - $hari) <= $detikperHari) {
            $deltaSecond = $tanggal - $hari;
            $dt1 = new DateTime("@0");
            $dt2 = new DateTime("@$deltaSecond");
            $hourTime = $dt1->diff($dt2)->format('%h');

//            print_r($dt2);
            $recent[$key]->tgl_publish = $hourTime;
            $recent[$key]->tipe = "hour";
        } else {
            $recent[$key]->tipe = "day";
        }
    }
//    print_r($recent);
//    die;

    $db->select("m_artikel.id, m_kategori_artikel.nama AS kategori,
                COUNT(m_kategori_artikel_id) AS jml")
        ->from("m_artikel")
        ->join("left join", "m_kategori_artikel", "m_artikel.m_kategori_artikel_id=m_kategori_artikel.id")
        ->groupby("m_kategori_artikel.id");
    $category = $db->findAll();
//    print_r($category);
//    die;

    return $this->view->render($response, 'frontend/blog.twig', [
        'page' => 'blog',
        'data' => $newsList,
        'kategori' => $category,
        'recentpost' => $recent,
    ]);
});

$app->get('/blog_details', function ($request, $response) {
    $db = $this->db;
    $params = $_GET;

    $db->select("m_artikel.id, m_artikel.judul, m_artikel.isi, m_artikel.detail_isi, m_artikel.foto")
        ->from("m_artikel")
        ->where("id", "like", $params["index"]);

    $models = $db->find();
//    print_r($models);
//    die;

    return $this->view->render($response, 'frontend/blog_details.twig', [
        'page' => 'blog_details',
        'data' => $models,
    ]);
});



