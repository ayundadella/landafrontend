<?php
/**
 * Validasi
 * @param array $data
 * @param array $custom
 * @return array
 */
function validasi($data, $custom = array())
{
    $validasi = array(
        "tanggal" => "required",
        "total" => "required",
        "m_customer_id" => "required",
        "status" => "required",
    );
    $cek = validate($data, $validasi, $custom);
    return $cek;
}

/**
 * Ambil detail t penjualan
 */
$app->get("/t_penjualan/view", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
        ->from("t_penjualan_det")
        ->where("t_penjualan_id", "=", $params["t_penjualan_id"]);
    $models = $db->findAll();
    return successResponse($response, $models);
});

/**
 * Filter ui select customer
 */
$app->get("/t_penjualan/customer", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
        ->from("m_customer")
        ->where("nama", "like", $params["nama"]);
    $models = $db->findAll();
//    print_r($models);
//    die;
    return successResponse($response, ["list" => $models]);
});

/**
 * Filter ui select barang
 */
$app->get("/t_penjualan/barang", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
        ->from("m_barang")
        ->where("nama", "like", $params["nama"]);
    $models = $db->findAll();
//    print_r($models);
//    die;
    return successResponse($response, ["list" => $models]);
});

/**
 * Ambil detail t penjualan
 */
$app->get("/t_penjualan/view/{id}", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $id = $request->getAttribute('id');
    $db->select("t_penjualan_det.*, m_barang.nama AS nama_barang")
        ->from("t_penjualan_det")
        ->join("left join", "m_barang", "m_barang.id=t_penjualan_det.m_barang_id")
        ->where("t_penjualan_id", "=", $id);
    $models = $db->findAll();
    foreach ($models as $key => $value) {
        $models[$key]->m_barang_id = [
            "id" => $value->m_barang_id,
            "nama" => $value->nama_barang
        ];
    }
    return successResponse($response, $models);
});

/**
 * Ambil semua t penjualan
 */
$app->get("/t_penjualan/index", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("
                t_penjualan.*,
                m_customer.nama AS nama_customer")
        ->from("t_penjualan")
    ->join("left join", "m_customer", "t_penjualan.m_customer_id=m_customer.id");
    /**
     * Filter
     */
    if (isset($params["filter"])) {
        $filter = (array)json_decode($params["filter"]);
        foreach ($filter as $key => $val) {
            $db->where($key, "LIKE", $val);
        }
    }
    /**
     * Set limit dan offset
     */
    if (isset($params["limit"]) && !empty($params["limit"])) {
        $db->limit($params["limit"]);
    }
    if (isset($params["offset"]) && !empty($params["offset"])) {
        $db->offset($params["offset"]);
    }
    $models = $db->findAll();
    foreach ($models as $key => $value){
        $models[$key]->m_customer_id = [
            "id" => $value->m_customer_id,
            "nama" => $value->nama_customer,
        ];
    }
    $totalItem = $db->count();
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);
});
/**
 * Save t penjualan
 */
$app->post("/t_penjualan/save", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    $validasi = validasi($data["data"]);
    if ($validasi === true) {
        try {
            $data['data']['tanggal'] = date("Y-m-d", strtotime($data['data']['tanggal']));
            $data['data']['m_customer_id'] = $data['data']['m_customer_id']['id'];
            if (isset($data["data"]["id"])) {
                $model = $db->update("t_penjualan", $data["data"], ["id" => $data["data"]["id"]]);
                $db->delete("t_penjualan_det", ["t_penjualan_id" => $data["data"]["id"]]);
            } else {
                $model = $db->insert("t_penjualan", $data["data"]);
            }
            /**
             * Simpan detail
             */
            if (isset($data["detail"]) && !empty($data["detail"])) {
                foreach ($data["detail"] as $key => $val) {
                    $detail["m_barang_id"] = $val["m_barang_id"]["id"];
                    $detail["jumlah"] = $val["jumlah"];
                    $detail["harga"] = $val["harga"];
                    $detail["t_penjualan_id"] = $model->id;
                    $db->insert("t_penjualan_det", $detail);
//                    print_r($detail);
//                    die;
                    if($model->status == "tersimpan"){
                        $db->run("UPDATE m_barang SET stok=stok-" . $val["jumlah"] . " WHERE id = " . $val["m_barang_id"]["id"]);
                    }
                }
            }
            return successResponse($response, $model);
        } catch (Exception $e) {
            return unprocessResponse($response, ["terjadi masalah pada server"]);
        }
    }
    return unprocessResponse($response, $validasi);
});
/**
 * Hapus t penjualan
 */
$app->post("/t_penjualan/hapus", function ($request, $response) {
    $data = $request->getParams();
    $db = $this->db;
    try {
        $model = $db->delete("t_penjualan", ["id" => $data["id"]]);
        $modelDetail = $db->delete("t_penjualan_det", ["t_penjualan_id" => $data["id"]]);
        return successResponse($response, $model);
    } catch (Exception $e) {
        return unprocessResponse($response, ["terjadi masalah pada server"]);
    }
    return unprocessResponse($response, $validasi);
});
