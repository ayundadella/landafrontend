<?php
/**
 * Menampilkan seluruh data mhs
 */

$app->get("/l_siswa/view", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("m_siswa.*,
                m_sekolah.nama AS sekolah_nama,
                m_sekolah.alamat AS sekolah_alamat,
                m_sekolah.no_telp AS sekolah_telepon,
                m_siswa.nama AS siswa_nama,
                m_siswa.nis AS siswa_nis,
                m_siswa.tanggal_lahir AS siswa_tanggal_lahir")
        ->from("m_sekolah")
        ->join("left join", "m_siswa", "m_siswa.m_sekolah_id=m_sekolah.id");
//        ->where("m_jurusan.is_deleted", "=", 0);

    if (isset($params["kelompokSiswa"]) && !empty($params["kelompokSiswa"])) {
        $db->where("m_sekolah.jenis", "=", $params["kelompokSiswa"]);
    }

    $models = $db->findAll();

    $result = [];
    foreach ($models as $key => $value) {
        $result[$value->m_sekolah_id] ["m_sekolah_id"] = $value->m_sekolah_id;
        $result[$value->m_sekolah_id] ["sekolah_nama"] = $value->sekolah_nama;
        $result[$value->m_sekolah_id] ["sekolah_alamat"] = $value->sekolah_alamat;
        $result[$value->m_sekolah_id] ["sekolah_telepon"] = $value->sekolah_telepon;
        $result[$value->m_sekolah_id] ["dataSiswa"] [] = $value;
    }
    $totalItem = $db->count();
    return successResponse($response, ["list" => $result, "totalItems" => $totalItem]);
});
$app->get("/l_siswa/sekolah", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
        ->from("m_sekolah")
        ->where("m_sekolah.jenis", "=", "jenis_sekolah");

    $models = $db->findAll();
    return successResponse($response, ["list" => $models]);
});


