<?php
/**
 * Menampilkan seluruh data mhs
 */

$app->get("/l_jurusan/view", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("m_jurusan.*,
                m_jurusan.nama AS jurusan_nama,
                m_jurusan.alamat AS jurusan_alamat,
                m_jurusan.no_telp AS jurusan_telepon,
                m_kelas.id AS m_kelas_id,
                m_kelas.nama AS kelas_nama,
                m_mahasiswa.nama AS mahasiswa_nama,
                m_mahasiswa.nim AS mahasiswa_nim,
                m_mahasiswa.alamat AS mahasiswa_alamat,
                m_mahasiswa.tanggal_lahir AS mahasiswa_tgl_lahir,
                t_kelompok_kelas.id AS kelompok_kelas,
                t_kelompok_kelas.m_jurusan_id AS kelompok_jurusan_id")
        ->from("m_jurusan")
        ->join("left join", "t_kelompok_kelas", "t_kelompok_kelas.m_jurusan_id=m_jurusan.id")
        ->join("left join", "m_kelas", "t_kelompok_kelas.m_kelas_id=m_kelas.id")
        ->join("left join", "t_kelompok_kelas_det", "t_kelompok_kelas_det.t_kelompok_kelas_id=t_kelompok_kelas.id")
        ->join("left join", "m_mahasiswa", "m_mahasiswa.id=t_kelompok_kelas_det.m_mahasiswa_id");
//      ->where("m_jurusan.is_deleted", "=", 0);

    if (isset($params["kelompokJurusan"]) && !empty($params["kelompokJurusan"])) {
        $db->where("m_jurusan.id", "=", $params["kelompokJurusan"]);
    }

    if (isset($params["kelompokKelas"]) && !empty($params["kelompokKelas"])) {
        $db->where("m_kelas.id", "=", $params["kelompokKelas"]);
    }

    $models = $db->findAll();
//    print_r($models);
//    die;

    $result = [];
    foreach ($models as $key => $value) {
        $result[$value->m_kelas_id] ["m_kelas_id"] = $value->m_kelas_id;
        $result[$value->m_kelas_id] ["jurusan_nama"] = $value->jurusan_nama;
        $result[$value->m_kelas_id] ["jurusan_alamat"] = $value->jurusan_alamat;
        $result[$value->m_kelas_id] ["jurusan_telepon"] = $value->jurusan_telepon;
        $result[$value->m_kelas_id] ["kelas_nama"] = $value->kelas_nama;

        if (!empty($value->mahasiswa_nim)) {
            $result[$value->m_kelas_id] ["dataMahasiswa"] [] = $value;
        }
    }
//    print_r($result);
//    die;

    $totalItem = $db->count();
    return successResponse($response, ["list" => $result, "totalItems" => $totalItem]);

});

$app->get("/l_jurusan/jurusan", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
        ->from("m_jurusan");

    $models = $db->findAll();
    return successResponse($response, ["list" => $models]);

});

$app->get("/l_jurusan/kelas/{id}", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $id = $request->getAttribute("id");
    $db->select("m_kelas.*")
        ->from("m_kelas")
        ->join("left join", "m_jurusan", "m_kelas.m_jurusan_id=m_jurusan.id")
        ->where("m_kelas.m_jurusan_id", "=", $id);

    $models = $db->findAll();
//    print_r($models);
//    die;
    return successResponse($response, ["list" => $models]);

});

//$app->get("l_jurusan/kelas", function ($request, $response) {
//    $data = $request->getParams();
//    $db = $this->db;
//
//    $db->select("*")
//    ->from("m_kelas");
//
//    if(!empty($data["kelompokJurusan"])){
//        $db->where("m_jurusan_id", "=", $data["kelompokJurusan"]);
//    }
//    if(!empty($data["kelompokKelas"])){
//        $db->where("nama", "LIKE", $data["kelompokKelas"]);
//    }
//    $models = $db->findAll();
//    return successResponse($response, $models);
//});


