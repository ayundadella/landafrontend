<?php
/**
 * Menampilkan seluruh data mhs
 */

$app->get("/l_mahasiswa/view", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("m_jurusan.*,
                m_jurusan.nama AS nama_jurusan,
                m_kelas.nama AS nama_kelas,
                m_mahasiswa.nama AS nama_mahasiswa,
                m_mahasiswa.nim AS nim_mahasiswa,
                m_mahasiswa.nama AS nama_mahasiswa,
                m_mahasiswa.tanggal_lahir AS tgl_lahir_mahasiswa")
        ->from("m_jurusan")
        ->join("left join", "t_kelompok_kelas", "t_kelompok_kelas.m_jurusan_id=m_jurusan.id")
        ->join("left join", "m_kelas", "t_kelompok_kelas.m_kelas_id=m_kelas.id")
        ->join("left join", "t_kelompok_kelas_det", "t_kelompok_kelas_det.t_kelompok_kelas_id=t_kelompok_kelas.id")
        ->join("left join", "m_mahasiswa", "m_mahasiswa.id=t_kelompok_kelas_det.m_mahasiswa_id");

    if (isset($params["jenisJurusan"]) && !empty($params["jenisJurusan"])) {
        $db->where("m_jurusan.id", "=", $params["jenisJurusan"]);
    }

    $models = $db->findAll();
    $totalItem = $db->count();
    return successResponse($response, ["list" => $models, "totalItems" => $totalItem]);

});

$app->get("/l_mahasiswa/jurusan", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
        ->from("m_jurusan");

    $models = $db->findAll();
    return successResponse($response, ["list" => $models]);

});

