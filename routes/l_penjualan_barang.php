<?php
/**
 * Menampilkan seluruh data penjualan barang
 */

$app->get("/l_penjualan_barang/view", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $tanggal = $params['tanggal'];
    $tanggal_int = strtotime($tanggal);
    $tanggal_awal = date('Y-m-01', $tanggal_int);
    $tanggal_akhir = date('Y-m-t', $tanggal_int);

    $db->select("m_barang.*,
                m_customer.nama AS nama_customer,
                m_barang.nama AS nama_barang,
                m_barang.satuan AS satuan_barang, 
                t_penjualan.tanggal AS tanggal_penjualan,
                t_penjualan_det.jumlah AS jumlah_jual,
                t_penjualan_det.harga AS harga_satuan")
        ->from("t_penjualan")
        ->join("left join", "m_customer", "t_penjualan.m_customer_id=m_customer.id")
        ->join("left join", "t_penjualan_det", "t_penjualan_det.t_penjualan_id=t_penjualan.id")
        ->join("left join", "m_barang", "t_penjualan_det.m_barang_id=m_barang.id")
        ->where("tanggal", ">=", $tanggal_awal)
        ->where("tanggal", "<=", $tanggal_akhir);


    if (isset($params["kelompokCustomer"]) && !empty($params["kelompokCustomer"])) {
        $db->where("m_customer.id", "=", $params["kelompokCustomer"]);
    }

    if (isset($params["kelompokBarang"]) && !empty($params["kelompokBarang"])) {
        $db->where("m_barang.id", "=", $params["kelompokBarang"]);
    }

    $models = $db->findAll();
    $total = 0;
    $jmlbrg = 0;
    $result = [];
    foreach ($models as $key => $value) {
        $subTotal = $value->jumlah_jual * $value->harga_satuan;
        $jmlbrg += $value->jumlah_jual;
        $total += $subTotal;
        $models[$key]->subTotal = $subTotal;

        //array luar
        $result["jumlah_barang"] = $jmlbrg;
        $result["total_harga"] = $total;
    }

    $totalItem = $db->count();
    if (isset($params['is_export']) && $params['is_export'] == 1) {
        ob_start();
        $xls = PHPExcel_IOFactory::load("format_excel/laporan_penjualan.xlsx");
        // get the first worksheet
        $sheet = $xls->getSheet(0);
//        echo "gggggg";
//        die;
        $sheet->getCell('D2')->setValue(date('F Y', strtotime($params['tanggal'])));
        $index = 5;
        $no = 1;

        foreach ($models as $key => $value) {
            $value = (array)$value;
            $sheet->getCell('A' . $index)->setValue($no++);
            $sheet->getCell('B' . $index)->setValue($value['tanggal_penjualan']);
            $sheet->getCell('C' . $index)->setValue($value['nama_customer']);
            $sheet->getCell('D' . $index)->setValue($value['nama_barang']);
            $sheet->getCell('E' . $index)->setValue($value['jumlah_jual']);
            $sheet->getCell('F' . $index)->setValue($value['satuan_barang']);
            $sheet->getCell('G' . $index)->setValue($value['harga_satuan']);
            $sheet->getCell('H' . $index)->setValue($value['subTotal']);

            $index++;
        }
        $sheet->getCell('D' . $index)->setValue('TOTAL');
        $sheet->getCell('E' . $index)->setValue($result['jumlah_barang']);
        $sheet->getCell('H' . $index)->setValue($result['total_harga']);

        $writer = new PHPExcel_Writer_Excel2007($xls);
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header("Content-Disposition: attachment;Filename=\"REKAP PENJUALAN BARANG PER BULAN " . date("F Y", strtotime($params['tanggal'])) . ".xlsx\"");

        ob_end_clean();
        $writer->save('php://output');
        exit;

    } elseif (isset($params['is_print']) && $params['is_print'] == 1) {
        $view = twigView();
        $content = $view->fetch("laporan/penjualan.html", [
            'data' => $models,
            'total' => $result,
            'month' => $params,
            'css' => modulUrl() . '/assets/css/style.css',
        ]);
        echo $content;
        echo '<script type="text/javascript">window.print();setTimeout(function () { window.close(); }, 500);</script>';
    }
//        print_r($models);
//        die;
    return successResponse($response, ["rincian" => $models, "totalsemua" => $result]);
//    print_r($models);
//    die;
});

$app->get("/l_penjualan_barang/barang", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
        ->from("m_barang");

    $models = $db->findAll();
    return successResponse($response, ["list" => $models]);

});

$app->get("/l_penjualan_barang/customer", function ($request, $response) {
    $params = $request->getParams();
    $db = $this->db;
    $db->select("*")
        ->from("m_customer");

    $models = $db->findAll();
    return successResponse($response, ["list" => $models]);
});
