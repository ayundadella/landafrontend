app.controller("tpenjualanCtrl", function ($scope, Data, $rootScope) {
    /**
     * Inialisasi
     */
    var tableStateRef;
    $scope.formtittle = "";
    $scope.displayed = [];
    $scope.form = {};
    $scope.is_edit = false;
    $scope.is_view = false;
    $scope.is_create = false;
    $scope.loading = false;
    /**
     * End inialisasi
     */
    $scope.callServer = function callServer(tableState) {
        tableStateRef = tableState;
        $scope.isLoading = true;
        var offset = tableState.pagination.start || 0;
        var limit = tableState.pagination.number || 10;
        var param = {
            offset: offset,
            limit: limit
        };
        if (tableState.sort.predicate) {
            param["sort"] = tableState.sort.predicate;
            param["order"] = tableState.sort.reverse;
        }
        if (tableState.search.predicateObject) {
            param["filter"] = tableState.search.predicateObject;
        }
        Data.get("t_penjualan/index", param).then(function (response) {
            $scope.displayed = response.data.list;
            tableState.pagination.numberOfPages = Math.ceil(
                response.data.totalItems / limit
            );
        });
        $scope.isLoading = false;
    };

    /**
     * Fungsi form detail
     * @param id
     */
    $scope.getDetail = function (id) {
        Data.get("t_penjualan/view?t_penjualan_id=" + id).then(function (response) {
            $scope.listDetail = response.data;
        });
    };
    $scope.listDetail = [{}];

    $scope.getDetail = function (form) { //detail penjualan
        console.log('test')
        console.log(form);
        Data.get("t_penjualan/view/" + form).then(function (data) {
            $scope.listDetail = data.data;
            console.log(data);
            $scope.getSubTotal();
        });
    }
    $scope.addDetail = function (val) {
        var comArr = eval(val);
        var newDet = {};
        val.push(newDet);
    };
    $scope.removeDetail = function (val, paramindex) {
        var comArr = eval(val);
        if (comArr.length > 1) {
            val.splice(paramindex, 1);
        } else {
            alert("Something gone wrong");
        }
    };

    /**
     * UI Select Nama Customer
     * @param query
     */
    $scope.cariCustomer = function (query) {
        console.log(query)
        if (query.length >= 3) {
            Data.get('t_penjualan/customer', {'nama': query}).then(function (response) {
                $scope.listCustomer = response.data.list;
            });
        }
    };

    /**
     * UI Select Daftar Barang
     * @param query
     */
    $scope.cariBarang = function (query) {
        console.log(query)
        if (query.length >= 3) {
            Data.get('t_penjualan/barang', {'nama': query}).then(function (response) {
                $scope.listBarang = response.data.list;
            });
        }
    };

    $scope.getSubTotal = function () {
        var total = 0;
        angular.forEach($scope.listDetail, function (value, key) {
            value.SubTotal = value.jumlah * value.harga;
            total = total + value.SubTotal;
        });
        $scope.form.total = total;
    };

    /**
     * CRUD
     * @param form
     */
    $scope.create = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.is_create = true;
        $scope.formtittle = "Form Tambah Data";
        $scope.form = {};
        $scope.listDetail = [{}];
    };
    $scope.update = function (form) {
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.formtittle = "Edit Data : " + form.tanggal;
        $scope.form = form;
        $scope.getDetail(form.id);
        $scope.form.tanggal = new Date(form.tanggal);
    };
    $scope.view = function (form) {
        $scope.is_edit = true;
        $scope.is_view = true;
        $scope.formtittle = "Lihat Data : " + form.tanggal;
        $scope.form = form;
        $scope.getDetail(form.id);
        $scope.form.tanggal = new Date(form.tanggal);
    };
    $scope.save = function (form, status) {
        $scope.loading = true;
        form.status = status;
        var params = {
            data: form,
            detail: $scope.listDetail
        }
        Data.post("t_penjualan/save", params).then(function (result) {
            if (result.status_code == 200) {
                $rootScope.alert("Berhasil", "Data berhasil disimpan", "success");
                $scope.cancel();
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }
            $scope.loading = false;
        });
    };
    $scope.cancel = function () {
        $scope.is_edit = false;
        $scope.is_view = false;
        $scope.is_create = false;
        $scope.callServer(tableStateRef);
    };
    $scope.delete = function (row) {
        if (confirm("Apa anda yakin akan Menghapus item ini ?")) {
            row.is_deleted = 0;
            Data.post("t_penjualan/hapus", row).then(function (result) {
                $scope.displayed.splice($scope.displayed.indexOf(row), 1);
            });
        }
    };
});
