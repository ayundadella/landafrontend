angular.module('app').controller('dashboardCtrl', function ($scope, Data, $state, UserService, $location) {
    var user = UserService.getUser();
    if (user === null) {
        $location.path('/login');

    }

    Data.get("dashboard/barChart").then(function (response) {
        $scope.barChart = {
            type: "hbar",
            'title': {
                text: "Daftar Barang Paling Laku"
            },
            plotarea: {
                'adjust-layout': true,
            },
            'scale-x': {
                label: {
                    text: "Nama Barang",
                },
                labels: response.data.nama_barang,
            },
            'scale-y': {
                label: {
                    text: "Jumlah"
                },
            },
            series: [
                {
                    values: response.data.jumlah_barang,
                }
            ]
        };
    });

    Data.get("dashboard/lineChart").then(function (response) {
        $scope.lineChart = {
            type: "line",
            'title': {
                text: "Penjualan Barang per Bulan"
            },
            plotarea: {
                stacked: true
            },
            label: {
                text: '# of Units'
            },
            crosshairX: {
                plotLabel: {
                    _padding: '10px 15px',
                    borderRadius: '3px',
                    color: '#5D7D9A',
                    padding: '10px',
                    backgroundColor: '#fff',
                    thousandsSeparator: ',',
                }
            },
            _legend: {
                cursor: 'hand',
                draggable: true
            },
            legend: {
                cursor: 'hand',
                draggable: true,
                highlightPlot: true,
                item: {
                    fontColor: '#373a3c',
                    fontSize: '12px'
                },
                toggleAction: 'remove',
                borderRadius: '5px',
                header: {
                    text: 'Kategori',
                    color: '#5D7D9A',
                    padding: '10px'
                }
            },
            'scale-x': {
                label: {
                    text: "Tahun 2020"
                },
                step: "month",
                transform: {
                    type: "date",
                    all: "%M"
                }
            },
            series: response.data.series
        };
    });
});

