app.controller("lpenjualanCtrl", function ($scope, Data, $rootScope) {
    $scope.filter = {};
    $scope.tampilkan = false;
    $scope.filter.bulan = new Date();

    $scope.getLaporan = function (filter) {
        console.log(filter);
        Data.get("l_penjualan/view", filter).then(function (data) {
            if (data.status_code == 200) {
                $scope.tampilkan = true;
                $scope.dataPenjualan = data.data.list;
                console.log($scope.dataPenjualan);
                console.log(data);
                $scope.Tanggal = data.data.listTanggal;
                $scope.barangperHari = data.data.hari;
                $scope.barangperBulan = data.data.bulan;
            } else {
                $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
            }

        });
    };

    Data.get("l_penjualan/barang").then(function (data) {
        if (data.status_code == 200) {
            $scope.listBarang = data.data.list;
            console.log($scope.listBarang);
            console.log(data);
        } else {
            $rootScope.alert("Terjadi Kesalahan", setErrorMessage(result.errors), "error");
        }
    });
});